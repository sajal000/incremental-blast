# Incremental BLAST
We develop an efficient way to redeem spent BLAST search effort by introducing the Incremental BLAST. The tool makes use of the previous BLAST search results as it conducts new searches on only the incremental part of the database, recomputes statistical metrics such as e-values and combines these two sets of results to produce updated results. We develop statistics for correcting e-values of any BLAST result against any arbitrary sequence database. The experimental results and accuracy analysis demonstrate that Incremental BLAST can provide search results identical to NCBI BLAST at a significantly reduced computational cost
![alt text][docs/iblast.jpg]
## Requirement for Incremental Blast**

1. Python3
2. BLAST+ command line tools   
(Alternatively install the command line tools from the source provided with this distribution)

## Install command-line BLAST+ command tools
`./NCBI-BLAST-installer.sh`  
Add BLAST+ executables to PATH (../ncbi-blast/ncbi-blast-2.8.1+-src-iBLAST/c++/ReleaseMT/bin)

---

## Install Incremental BLAST

`./IncrementalBLAST-installer.sh`


## Running Incremental BLAST

`python3 iBLAST.py "blastp -db nr -query query.fasta -outfmt 5 -out result.xml"`

## Merging two results
```
 python BlastpMergerModule.py input1.xml input2.xml output.xml 
 python BlastnMergerModule.py input1.xml input2.xml output.xml 
```
## Merging N results
` python BlastpMergerModuleX.py 3 input1.xml input2.xml input3.xml output.xml`

## View Examples using Python Notebook Viewer  
Change from default viewer to **IPython Notebook**  
1. [Example of iBLAST for blastn (Karlin-Altschul Statistics)](https://bitbucket.org/sajal000/incremental-blast/src/master/examples/iBLAST%20Demonstration%20for%20blastn%20(Karlin-Altschul%20Statistics).ipynb?viewer=nbviewer)  
2. [Example of iBLAST for blastp (Spouge Statistics)](https://bitbucket.org/sajal000/incremental-blast/src/master/examples/iBLAST%20Demonstration%20for%20blastp%20(Spouge%20Statistics).ipynb)